package com.kubota.messaging.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.kubota.messaging.topology.MessageTopology;

@SpringBootApplication
public class ConsumerApplication {
	
	 private static String kafkaBrokerEndpoint = null;
	    private static String rawDataInputTopic = null;
	    private static String cleansedDataOutputTopic = null;

	public static void main(String[] args) {
		
		kafkaBrokerEndpoint = "localhost:9092";
    	rawDataInputTopic = "raw_data_storm_in";
    	cleansedDataOutputTopic = "cleansed_data_storm_out";

        MessageTopology.createAndRunDataCleansingTopology(kafkaBrokerEndpoint, rawDataInputTopic, cleansedDataOutputTopic);
		
		
		SpringApplication.run(ConsumerApplication.class, args);
	}
}
