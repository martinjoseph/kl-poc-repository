package com.kubota.messaging.constants;

public class MessagingConstants {
	
	  public static String DATA_CLEANSING_SPOUT = "data_spout";
	    public static String DATA_CLEANSING_BOLT = "data_cleansing_bolt";
	    public static String DATA_CLEANSED_WRITER_BOLT = "data_cleansed_writer_bolt";
	    public static String DATA_TUPLE_KEY = "key";
	    public static String DATA_TUPLE_RECORD = "record";
	    public static String DATA_TUPLE_OFFSET = "offset";
	    public static String DATA_SCRUBBED_TUPLE_RECORD = "scrubbed_data_record";

}
