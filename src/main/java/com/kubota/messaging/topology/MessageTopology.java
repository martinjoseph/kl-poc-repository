package com.kubota.messaging.topology;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

import com.kubota.messaging.bolt.DataCleansingBolt;
import com.kubota.messaging.bolt.DataKafkaWriterBolt;
import com.kubota.messaging.constants.MessagingConstants;
import com.kubota.messaging.spout.MessageSpout;



public class MessageTopology {
	
	 public static void createAndRunDataCleansingTopology(String kafkaBroker, String rawDataInputTopic, String cleansedDataOutputTopic) {

	        TopologyBuilder dataTopologyBuilder = new TopologyBuilder();

	        dataTopologyBuilder.setSpout(MessagingConstants.DATA_CLEANSING_SPOUT, MessageSpout.createMessageSpout(kafkaBroker, rawDataInputTopic), 1);
	        
	       
	        dataTopologyBuilder.setBolt(MessagingConstants.DATA_CLEANSING_BOLT, new DataCleansingBolt(), 1).shuffleGrouping(MessagingConstants.DATA_CLEANSING_SPOUT);
	        dataTopologyBuilder.setBolt(MessagingConstants.DATA_CLEANSED_WRITER_BOLT, DataKafkaWriterBolt.createDataKafkaWriterBolt(kafkaBroker, cleansedDataOutputTopic)).shuffleGrouping(MessagingConstants.DATA_CLEANSING_BOLT);

	        LocalCluster localCluster = new LocalCluster();
	        Config conf = new Config();
	        conf.setDebug(false);
	        // submit the topology to local cluster
	        localCluster.submitTopology("MessageTopology", conf, dataTopologyBuilder.createTopology());
	    }

}
