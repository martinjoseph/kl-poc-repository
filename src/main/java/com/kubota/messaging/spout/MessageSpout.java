package com.kubota.messaging.spout;

import java.util.Arrays;

import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;
import org.apache.storm.tuple.Fields;

import com.kubota.messaging.constants.MessagingConstants;


public class MessageSpout {
	
	  public static KafkaSpout createMessageSpout(String kafkaBrokerEndpoint, String rawDataInputTopic) {

		  KafkaSpoutConfig dataSpoutConfig = KafkaSpoutConfig.builder(kafkaBrokerEndpoint, rawDataInputTopic)
	                .setGroupId("data_storm_spout")
	                .setFirstPollOffsetStrategy(KafkaSpoutConfig.FirstPollOffsetStrategy.UNCOMMITTED_EARLIEST)
	                .setRecordTranslator(consumerRecord -> {
	                    return Arrays.asList(consumerRecord.offset(), consumerRecord.key(), consumerRecord.value());
	                }, new Fields(MessagingConstants.DATA_TUPLE_OFFSET, MessagingConstants.DATA_TUPLE_KEY, MessagingConstants.DATA_TUPLE_RECORD))
	                .build();


	        return new KafkaSpout<>(dataSpoutConfig);
	    }

}
