package com.kubota.messaging.bolt;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.storm.kafka.bolt.KafkaBolt;
import org.apache.storm.kafka.bolt.mapper.FieldNameBasedTupleToKafkaMapper;
import org.apache.storm.kafka.bolt.selector.DefaultTopicSelector;

import com.kubota.messaging.constants.MessagingConstants;

import java.util.Properties;


public class DataKafkaWriterBolt {

   
    public static KafkaBolt createDataKafkaWriterBolt(String kafkaBrokerEndpoint, String cleansedDataOutputTopic) {

        KafkaBolt<String, String> cleansedDataWriterBolt = new KafkaBolt<String, String>().withProducerProperties(getKafkaProducerProperties(kafkaBrokerEndpoint))
                .withTopicSelector(new DefaultTopicSelector(cleansedDataOutputTopic))
                .withTupleToKafkaMapper(new FieldNameBasedTupleToKafkaMapper<String, String>(MessagingConstants.DATA_TUPLE_KEY, MessagingConstants.DATA_SCRUBBED_TUPLE_RECORD));

        return cleansedDataWriterBolt;
    }

    private static Properties getKafkaProducerProperties(String kafkaBrokerEndpoint) {

        Properties kafkaProducerProp = new Properties();

        kafkaProducerProp.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokerEndpoint);
        kafkaProducerProp.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProducerProp.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProducerProp.put(ProducerConfig.CLIENT_ID_CONFIG, "data_storm_bolt");

        return kafkaProducerProp;
    }
}
