package com.kubota.messaging.bolt;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.kubota.messaging.constants.MessagingConstants;

import java.util.Map;


public class DataCleansingBolt extends BaseRichBolt {

    private OutputCollector collector;


    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    /**
     * Scrubs the raw  data record received as a Tuple by either dropping the invalid records or reformatting the record based on the RegEx pattern
     * Emits the cleansed Tuple ( Record Key,  Record Value)
     *
     * @param recordTuple
     */
    @Override
    public void execute(Tuple recordTuple) {

        String record = recordTuple.getString(2);

        // Drop the header record that defines the attribute names
        if (!(record.isEmpty() || record.contains("member_id") || record.contains("Total amount funded in policy code"))) {

            // Few records have emp_title with comma separated values resulting in records getting rejected.
            String scrubbedRecord = record.replace(", ", "|").replaceAll("[a-z],", "");
            collector.emit(recordTuple, new Values(recordTuple.getString(1), scrubbedRecord));
            collector.ack(recordTuple);

        } else {

            System.out.println("Invalid  Record dropped at offset --> " + recordTuple.getLong(0) + " | " + recordTuple.getString(2));
            collector.ack(recordTuple);
        }

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	 declarer.declare(new Fields(MessagingConstants.DATA_TUPLE_KEY, MessagingConstants.DATA_SCRUBBED_TUPLE_RECORD));
    }
}
